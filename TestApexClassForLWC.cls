public with sharing class TestApexClassForLWC {

    @AuraEnabled(cacheable=true)
    public Static List<SObject> getAllRecords(String recordIdA) {
        List<Object_C__c> allRecordsC = new List<Object_C__c>();
        List<Object_B__c> allRecordsB = new List<Object_B__c>();
        List<SObject> finalList = new List<SObject>();

        allRecordsC = [SELECT Id, Name FROM Object_C__c WHERE Object_B_lookup__r.Object_A_lookup__r.Id = :recordIdA];
        allRecordsB = [SELECT Id, Name FROM Object_B__c WHERE Object_A_lookup__r.Id = :recordIdA];

        finalList.addAll(allRecordsC);
        finalList.addAll(allRecordsB);

        System.debug('All Records: ' + finalList);
        return finalList;
    }
}