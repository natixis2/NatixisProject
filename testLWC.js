import { LightningElement, api, wire, track } from "lwc";
import getAllRecords from "@salesforce/apex/TestApexClassForLWC.getAllRecords"; 

export default class TestClassForLWC_Apex extends LightningElement{
    @api recordId;
    @track finalData = [];
    @track finalDataTest = [];
    @track record;
    @track nextIteration = 0;
    @track startPoint;
    @track totalRecords;
    tableElement;
    loadMoreStatus;
    numberOfRows = 10;
    maxRows=1000;

    columns = [
        { label: 'Name', fieldName: 'Name' }
    ];

    @wire(getAllRecords, {recordIdA: '$recordId'})
    WireDataRecords({error, data }){
        console.log('recordId: ' + this.recordId);
        console.log('All data: ' + this.data);
        
        if(data){
            if (data.length >= this.numberOfRows){
                for(let i=0; i<this.numberOfRows; i++){
                    this.finalData.push(data[i]);
                }
                this.nextIteration++;
                this.record = data;
            }else{
                for(let i=0; i<data.length; i++){
                    this.finalData.push(data[i]);
                }
                this.record = data;
            }
        }else if (error) {
            console.log('An error has occurred:');
            console.log(error);
            // handle your error.
        }
        console.log('finalData: ' + finalData);
    }
    
    loadMoreData(event) {
        //Display a spinner to signal that data is being loaded
        if(event.target){
            event.target.isLoading = true;
        }
        this.tableElement = event.target;
        //Display "Loading" when more data is being loaded
        this.loadMoreStatus = 'Loading';
        this.startPoint = this.numberOfRows*this.nextIteration;
        this.endPoint = this.startPoint + this.numberOfRows;

        if(this.record.data.length > this.endPoint){            
            for(let i=this.startPoint; i<this.endPoint; i++){
                this.finalData.push(this.records.data[i]);
            }
            this.loadMoreStatus = '';
            this.totalRecords = this.finalData.length; 
        }

        if (this.finalData.length  >= this.maxRows) {
            this.tableElement.enableInfiniteLoading = false;
            this.loadMoreStatus = 'No more data to load';
        }
        
        if(this.tableElement){
            this.tableElement.isLoading = false;
        } 
    }

    //Debug button
    handleGetRecords(){
        getAllRecords({recordIdA: '$recordId'});

        console.log('Hello World');
        console.log('records ' + this.records.data);
    }
}